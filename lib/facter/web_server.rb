# puppet facts --environment production | grep web_server
Facter.add(:web_server) do
  setcode do
    if Integer( Facter::Core::Execution.execute('ps -acx|grep nginx|wc -l') ) > 0
      $nginx = 'Installed [O]'
    else
      $nignx = 'Not Installed [X]'
    end

    if Integer( Facter::Core::Execution.execute('ps -acx|grep apache|wc -l') ) > 0
      $apache = 'Installed [O]'
    else
      $apache = 'Not Installed [X]'
    end

    "Nginx is " + $nginx + " | Apache is " + $apache
  end
end