require 'spec_helper'

describe 'Webserver::Port' do
  it { is_expected.to allow_value(1) }
  it { is_expected.to allow_value(80) }
  it { is_expected.to allow_value(8080) }
  it { is_expected.to allow_value(9000) }
  it { is_expected.to allow_value(65536) }

  it { is_expected.not_to allow_value(:undef) }
  it { is_expected.not_to allow_value(0) }
  it { is_expected.not_to allow_value(-1) }
  it { is_expected.not_to allow_value('80') }
  it { is_expected.not_to allow_value(65537) }
  it { is_expected.not_to allow_value(100000) }
end