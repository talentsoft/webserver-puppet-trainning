  
# frozen_string_literal: true

require 'spec_helper'

describe 'webserver' do

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to contain_notify('Install nginx') }

      it { is_expected.to compile }
      it { is_expected.to create_class('nginx') }
      it { is_expected.to contain_package('nginx') }
      it { is_expected.to contain_class('webserver') }

      it { is_expected.to contain_service('nginx').with_ensure('running').with_enable(true) }

      it { is_expected.to contain_file('/var/www/localhost/index.html').that_requires(['File[/var/www/localhost/]']) }
      
    end
  end
end