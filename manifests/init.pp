# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include webserver
class webserver(
  Optional[Webserver::Port] $port_number = 8080,
  String $site_location = '/var/www/localhost/',
  String $host = 'testhost'
){

  notify { 'Install nginx': }

  include nginx

  # https://puppet.com/docs/puppet/5.5/custom_types.html ???
  nginx::resource::server { $host:
    listen_port => $port_number,
    www_root    => $site_location,
  }

  file { $site_location:
    ensure => 'directory',
  }

  $time_utc = generate('/usr/bin/date','+%s')

  file { '/etc/hosts':
    ensure  => file,
    content => epp('webserver/hosts.epp', {
      'host_name' => $host
    }),
    backup  => 'true',
  }

  file { "${site_location}index.html":
    ensure  => file,
    content => epp('webserver/index.html.epp', {
      'page_title'    => 'Cutom Nginx title',
      'date_time'     => Timestamp.new().strftime('%Y-%m-%dT%H:%M:%S%:z'),
      'site_location' => $site_location,
      'port_number'   => $port_number,
    }),
    require => File[$site_location]
  }

  # Reverse proxy error
  # ------------------------------------------------
  # nginx::resource::server { 'localhost':
  #   listen_port => 8081
  # }

  # See example on:
  # https://github.com/voxpupuli/puppet-nginx/blob/master/manifests/resource/upstream.pp
  # ------------------------------------------------
  # nginx::resource::upstream { 'puppet_rack_app':
  #   members => {
  #     'localhost:3000' => {
  #       server => 'localhost',
  #       port   => 3000,
  #       weight => 1,
  #     },
  #     'localhost:3001' => {
  #       server => 'localhost',
  #       port   => 3001,
  #       weight => 1,
  #     },
  #     'localhost:3002' => {
  #       server => 'localhost',
  #       port   => 3002,
  #       weight => 2,
  #       },
  #   },
  # }

  # nginx::resource::server { 'rack.puppetlabs.com':
  #   proxy       => 'http://puppet_rack_app',
  # }

}
